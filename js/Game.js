var _question = null;

class Game {
  constructor() {}


  init(){
    if (localStorage.getItem('question') == undefined) {
      localStorage.setItem('question', 0);
    }
    this.loadQuestion(localStorage.getItem('question'));
  }

  loadQuestion(index) {
    $.ajax({
      url: `http://localhost:3000/questions/${index}`,
      type: 'get',
      error: function(XMLHttpRequest, textStatus, errorThrown){
        console.log("Can't load questions!");
        _question = null;
      },
      success: function(data){
        _question = data;
      }
    })
    .done(function(data) {
      game.displayQuestion();
    });
  }

  displayQuestion() {
    $('#subboard').empty();
    $('#next_question').remove();
    if(_question) {
      $('#subboard').append(`<span id="question_counter">${_question.id + 1}/${_lastQuestion}</span>
      <div class="container">
        <div class="row">
          <figure>
            <img src="${_question.img}" alt="photo" width="80%" height="auto">
          </figure>
        </div>
        <div class="row">
          <div class="col"><p>${_question.text}</p></div>
        </div>
        <div class="row card-deck">
          <div class="card card_hover answer">${_question.answers[0]}</div>
          <div class="card card_hover answer">${_question.answers[1]}</div>
        </div>
        <div class="row card-deck">
          <div class="card card_hover answer">${_question.answers[2]}</div>
          <div class="card card_hover answer">${_question.answers[3]}</div>
        </div>
      </div>`);
    }

    // Check the answer
    $('.answer').on('click', function() {
      var selected = $(this).text();
      if (selected == _question.correct) {
        score.addPoints(_question.points);
        $(this).addClass('correct_answer');
      } else {
        $(this).addClass('wrong_answer');
        $('.answer').each( function(index){
          if($(this).text() == _question.correct){
            $(this).addClass('correct_answer');
          }
        })
      }
      $('.answer').each( function(index){
        $(this).off( "click");
        $(this).removeClass('card_hover');
      })
      game.showNext();
    })
  }

  showNext() {

    if(+_question.id + 1 >= _lastQuestion) {
      $('#game').append(`<button id="next_question" class="common_btn">End</button>`);
      $('#next_question').on('click', function(){
        game.gameOver();
      });
    } else {
      _question.id++;
      localStorage.setItem('question', +_question.id);
      $('#game').append(`<button id="next_question" class="common_btn">Next</button>`);
      $('#next_question').on('click', function(){
        game.loadQuestion(_question.id);
        game.displayQuestion();
      });
    }
  }

  gameOver() {
    localStorage.setItem('question', undefined);
    $('#subboard').empty();
    $('#next_question').remove();

    $('#subboard').append("<h2>Enter your name</h2><input id=\"player-name\" type=\"text\"><p>And press enter</p>");
    $(document).keypress(function(e) {
      if(e.which == 13) {
        var player = $('#player-name').val();
        localStorage.setItem('player', player);
        $(document).off('keypress');
        score.add();
        score.showResult();
      }
    });
  }

  questionsSize() {
    $.get('http://localhost:3000/questions', function(data, status){
      _lastQuestion = data.length;
    })
  }
}
