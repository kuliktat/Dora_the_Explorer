class Score {
  constructor() {}

  // Displays score in ordered list in DESC order
  display() {
    $.get('http://localhost:3000/scores?_sort=score&_order=desc', function(data, status){
      $.each( data, function( index, val ){
        $('#scores').append("<tr><th scope=\"row\">" + (index+1) + "</th><td>" + val.name + "</td><td>" + val.score + "</td></tr>")
      });
    })
  }

  // Adds new score and refres leaderboard
  add() {
    if (result > 100) {
      console.log("Cheater! You can't get more then 100 points!");
      return false;
    }
    var data = {name: localStorage.getItem('player'), score: result};
    $.post("http://localhost:3000/scores", data, function(){
        $('#scores').empty();
        score.display();
    });
    localStorage.setItem('score', result);
    result = 0;
  }

  addPoints(points) {
    result += points;
  }

  showResult() {
    $('#subboard').empty();
    $('#next_question').remove();
    $('#subboard').append(`
      <figure class="pie-figure">
      <svg width="100" height="100" class="chart">
        <circle r="25" cx="50" cy="50" class="pie"/>
      </svg>
      <figcaption>
        <ul class="figure-key-list" aria-hidden="true" role="presentation">
          <li>
            <span class="shape-circle shape-right"></span> Right answers
          </li>
          <li>
            <span class="shape-circle shape-wrong"></span> Wrong answers
          </li>
        </ul>
      </figcaption>
    </figure>
    <p>Score: ${localStorage.getItem('score')}</p>
    `);

    var pie = new Pie(localStorage.getItem('score'));
    pie.setPieChart();
  }

}


class Pie {

  constructor(score) {
    this.score = score;
  }

  setPieChart(){
  	document.querySelector('.pie').style.strokeDasharray = (this.score * 1.58) + ' ' + 158;
  }

}
