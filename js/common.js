// init google map
function initMap() {
  var uluru = {lat: 50.102695, lng: 14.393056};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

var score = new Score();
var game = new Game();

var result = 0;
var _lastQuestion = 0;


function replay() {
  $('#subboard').empty();
  localStorage.clear();
  $('#next_question').remove();
  $('#subboard').append("<input id=\"start\" class=\"button\" type=\"button\" value=\"Play\">");
  $('#start').click(function(){
    var game = new Game();
    game.init();
  })
}

function gameover() {
  score.add();
}


$(document).ready(function(){

    // Display leaderboard
    score.display();
    game.questionsSize();

    if(localStorage.getItem('question') != undefined){
      $('#start').attr("value", "Continue");
    }

    $('#start').click(function(){
      game.init();
    })

    // Replay button
    $(".replay").click(function(){
      replay();
    })

    // Toggle secondary blocks
    $('#options').click(function(){
      $('.full-screen-dn').toggle( "slow" );
    })

});
