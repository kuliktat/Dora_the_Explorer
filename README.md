# About 'Geographic Quiz'

Simple javascript game.
Try to answer all 10 questions to earn 100 points and be on the top of the leaderboard!

# Installation

You need **json-server** on your computer. To install it run:

```
npm i json-server
```

[Json-server DOCS](https://github.com/typicode/json-server) - simple json API, used to store questions and scores.

Then you need to clone this repo into local folder:

```
cd YOUR_FOLDER
git clone git@gitlab.fel.cvut.cz:kuliktat/Dora_the_Explorer.git
cd Dora_the_Explorer
```

Download third-party libs:

```
npm install
```

Run json server (you need to be in folder with db.json):

```
json-server db.json
```

Now you can run index.html in your browser.

# Used technologies

- [JQuery](https://jquery.com/)
- [Bootstrap](https://getbootstrap.com/)
- [Google maps](https://www.google.com/maps)
- [NPM](https://www.npmjs.com/)
- [FontAwesome](https://fontawesome.com/)
- [JsonServer](https://github.com/typicode/json-server)